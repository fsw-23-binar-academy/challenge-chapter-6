var express = require("express");
var router = express.Router();

const { index, doLogin, logout } = require("./controller");

router.get("/", index);
router.post("/", doLogin);
router.get("/logout", logout);

module.exports = router;
