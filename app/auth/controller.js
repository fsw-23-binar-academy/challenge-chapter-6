const validateLogin = (email, password) => {
  if (email === "dev@dev.com" && password === "dev") {
    return true;
  } else {
    return false;
  }
};

module.exports = {
  index: (req, res) => {
    try {
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");
      const alert = { message: alertMessage, status: alertStatus };

      if (req.session.user) {
        res.redirect("/users");
      } else {
        res.render("pages/login", { alert });
      }
    } catch (e) {
      req.flash("alertMessage", `${e.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/");
    }
  },
  doLogin: (req, res) => {
    try {
      const { email, password } = req.body;

      if (validateLogin(email, password)) {
        req.session.user = {
          ...req.body,
          name: "Hanafiq P",
          isLoggedIn: true,
          role: "superadmin",
        };
        res.redirect("/users");
      } else {
        req.flash("alertMessage", "Email or Password is wrong");
        req.flash("alertStatus", "danger");
        res.redirect("/");
      }
    } catch (e) {
      req.flash("alertMessage", `${e.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/");
    }
  },
  logout: (req, res) => {
    try {
      req.session.destroy();
      res.redirect("/");
    } catch (e) {
      req.flash("alertMessage", `${e.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/");
    }
  },
};
