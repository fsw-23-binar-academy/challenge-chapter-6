var express = require("express");
var router = express.Router();

const {
  index,
  addUsers,
  addUsersToDB,
  editUsers,
  editUsersToDB,
  deleteUsersFromDB,
} = require("./controller");
const { isLoggedIn } = require("../../middleware/auth");

router.use(isLoggedIn);
/* this is routing for page. */
router.get("/", index);
router.get("/add", addUsers);
router.get("/edit/:id", editUsers);

// routing for action because the requirement is only use 1 endpoint to crud
router.post("/", addUsersToDB);
router.put("/:id", editUsersToDB);
router.delete("/:id", deleteUsersFromDB);

module.exports = router;
