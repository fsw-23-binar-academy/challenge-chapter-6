const Model = require("../../models");
const {
  user_game: UserGame,
  user_biodata: UserBiodata,
  user_game_history: UserGameHistory,
} = Model;
module.exports = {
  index: async (req, res) => {
    try {
      const alertMessage = req.flash("alertMessage");
      const alertStatus = req.flash("alertStatus");

      const alert = { message: alertMessage, status: alertStatus };

      const data = await UserGame.findAll({
        where: {
          deleted_at: null,
        },
        include: [
          {
            model: UserBiodata,
            as: "biodata",
          },
          {
            model: UserGameHistory,
            as: "history",
          },
        ],
      });
      // res.status(200).json(data);
      res.render("template", {
        pages: "/users/index",
        logic: "users",
        data,
        alert,
        userName: req.session.user.name,
      });
    } catch (e) {
      req.flash("alertMessage", `${e.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/users");
    }
  },
  addUsers: (req, res) => {
    try {
      res.render("template", {
        pages: "/users/add",
        logic: "addUsers",
        data: {},
        alert: {},
        userName: req.session.user.name,
      });
    } catch (e) {
      req.flash("alertMessage", `${e.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/users");
    }
  },
  addUsersToDB: async (req, res) => {
    try {
      const { name, email, password, username, address, phone_number } =
        req.body;
      console.log(req.body);
      const response = await UserGame.create({
        username,
        email,
        password,
      });
      const user_id = response.dataValues.id;
      if (user_id) {
        await UserBiodata.create({
          user_id,
          name,
          address,
          phone_number,
        });

        req.flash("alertMessage", "Success add user");
        req.flash("alertStatus", "success");
        res.redirect("/users");
      }
    } catch (e) {
      req.flash("alertMessage", `${e.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/users");
    }
  },
  editUsers: async (req, res) => {
    try {
      const { id } = req.params;
      const data = await UserGame.findOne({
        where: { id },
        include: [
          {
            model: UserBiodata,
            as: "biodata",
          },
        ],
      });

      res.render("template", {
        pages: "/users/edit",
        logic: "editUsers",
        data,
        alert: {},
        userName: req.session.user.name,
      });
    } catch (e) {
      req.flash("alertMessage", `${e.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/users");
    }
  },
  editUsersToDB: async (req, res) => {
    try {
      const { id } = req.params;
      const { name, email, password, username, address, phone_number } =
        req.body;
      await UserGame.update(
        {
          username,
          email,
          password,
        },
        {
          where: { id },
        }
      );
      await UserBiodata.update(
        {
          name,
          address,
          phone_number,
        },
        {
          where: { user_id: id },
        }
      );
      req.flash("alertStatus", "success");
      req.flash("alertMessage", `Success edit user ${name}`);
      res.redirect("/users");
    } catch (e) {
      req.flash("alertMessage", `${e.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/users");
    }
  },
  deleteUsersFromDB: async (req, res) => {
    try {
      const { id } = req.params;
      await UserGame.update(
        {
          deleted_at: new Date(),
        },
        {
          where: { id },
        }
      );

      await UserBiodata.update(
        {
          deleted_at: new Date(),
        },
        {
          where: { user_id: id },
        }
      );

      req.flash("alertStatus", "success");
      req.flash("alertMessage", `Success delete user`);
      res.redirect("/users");
    } catch (e) {
      req.flash("alertMessage", `${e.message}`);
      req.flash("alertStatus", "danger");
      res.redirect("/users");
    }
  },
};
