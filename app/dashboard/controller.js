module.exports = {
  index: (req, res) => {
    try {
      res.render("template", {
        pages: "dashboard",
        logic: "dashboard",
        data: {},
        alert: {},
        userName: req.session.user.name,
      });
    } catch (e) {
      console.log(e);
    }
  },
};
