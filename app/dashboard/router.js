var express = require("express");
var router = express.Router();

const { index } = require("./controller");
const { isLoggedIn } = require("../../middleware/auth");

router.use(isLoggedIn);
router.get("/", index);

module.exports = router;
