"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    let data = [];

    const stateWin = ["win", "lose", "draw"];

    for (let i = 0; i <= 100; i++) {
      data.push({
        user_id: Math.floor(Math.random() * 11),
        is_win: stateWin[Math.floor(Math.random() * stateWin.length)],
        created_at: new Date(),
        updated_at: new Date(),
      });
    }
    return queryInterface.bulkInsert("user_game_histories", data);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    queryInterface.bulkDelete("user_game_histories", null, {});
  },
};
