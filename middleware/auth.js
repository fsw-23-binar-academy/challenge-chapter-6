module.exports = {
  isLoggedIn: (req, res, next) => {
    if (req.session.user) {
      next();
    } else {
      req.flash("alertMessage", "You must login first");
      req.flash("alertStatus", "danger");
      res.redirect("/");
    }
  },
};
