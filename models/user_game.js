"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ user_biodata, user_game_history }) {
      // define association here
      user_game.hasOne(user_biodata, {
        foreignKey: "user_id",
        as: "biodata",
      });

      user_game.hasMany(user_game_history, {
        foreignKey: "user_id",
        as: "history",
      });
    }
  }
  user_game.init(
    {
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "created_at",
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "updated_at",
      },
      deleted_at: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "user_game",
    }
  );
  return user_game;
};
