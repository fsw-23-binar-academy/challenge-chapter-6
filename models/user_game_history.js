"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ user_game_history, user_game }) {
      // define association here
      user_game_history.belongsTo(user_game, {
        foreignKey: "user_id",
      });
    }
  }
  user_game_history.init(
    {
      user_id: DataTypes.INTEGER,
      is_win: DataTypes.ENUM("win", "draw", "lose"),
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "created_at",
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: new Date(),
        field: "updated_at",
      },
    },
    {
      sequelize,
      modelName: "user_game_history",
      tableName: "user_game_histories",
    }
  );
  return user_game_history;
};
